package be.rivendale.list;

import be.rivendale.parser.Parser;
import org.junit.Test;

public class ListParserTest extends AbstractParserTest {
    @Test
    public void parseListVariations() throws Exception {
        assertParsable("[]");
        assertParsable("[a]");
        assertParsable("[a, b]");
        assertParsable("[a, []]");
        assertParsable("[[a], b]");
        assertParsable("[a, b, c, d]");
        assertParsable("[a, [b, c], d]");
        assertParsable("[a, [b, [c, d]], e]");
        assertParsable("[a=b, c, [d, e=f], g]");
    }

    @Override
    protected Parser createParser(String sentence) {
        return new FixedListParser(sentence);
    }
}
