package be.rivendale.list;

import be.rivendale.math.MathParser;
import be.rivendale.parser.Parser;
import org.junit.Test;

public class MathParserTest extends AbstractParserTest {
    @Test
    public void number() throws Exception {
        assertParsable("5");
    }

    @Test
    public void simpleTerm() throws Exception {
        assertParsable("5 * 8");
    }

    @Test
    public void groupedTerm() throws Exception {
        assertParsable("(5 * 8)");
    }

    @Test
    public void groupedCombinationTerms() throws Exception {
        assertParsable("(5 * (3 + 2))");
    }

    @Test
    public void complexCombinationTerm() throws Exception {
        assertParsable("(5 * (8 + (3 / 7))) - 2");
    }

    @Test
    public void ungrouped() throws Exception {
        assertParsable("5 + 3 * 2");
    }

    @Test
    public void groupedAndUngroupedCombination() throws Exception {
        assertParsable("(5 + 3) / 2 * (7 * (3 - 1))");
    }

    @Override
    protected Parser createParser(String sentence) {
        return new MathParser(sentence);
    }
}
