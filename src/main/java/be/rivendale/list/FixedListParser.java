package be.rivendale.list;

import be.rivendale.list.ListLexer;
import be.rivendale.list.ListTokenType;
import be.rivendale.parser.Parser;
import be.rivendale.parser.lexer.Lexer;

import static be.rivendale.list.ListTokenType.*;

public class FixedListParser extends Parser<ListTokenType> {
    public FixedListParser(String sentence) {
        super(new ListLexer(sentence), 2);
    }

    @Override
    public void parse() {
        list();
        match(eof);
    }

    private void list() {
        match(leftBracket);
        if(isEqual(variable, leftBracket)) {
            items();
        }
        match(rightBracket);
    }

    private void items() {
        item();
        while(isEqual(comma)) {
            match(comma);
            item();
        }
    }

    private void item() {
        if(isEqual(leftBracket)) {
            list();
        } else if(isEqual(1, variable) && isEqual(2, equals)) {
            assignment();
        } else {
            variable();
        }
    }

    private void variable() {
        match(variable);
    }

    private void assignment() {
        match(variable);
        match(equals);
        match(variable);
    }
}
