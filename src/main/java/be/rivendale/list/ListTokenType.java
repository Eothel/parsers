package be.rivendale.list;

public enum ListTokenType {
    leftBracket,
    rightBracket,
    equals,
    comma,
    variable,
    eof,
}
