package be.rivendale.list;

import be.rivendale.parser.InvalidPeekPosition;
import be.rivendale.parser.ParserException;
import be.rivendale.parser.TokenMismatchException;
import be.rivendale.parser.lexer.Token;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.Stack;

import static be.rivendale.list.ListTokenType.*;

public class BacktrackingListParser extends Base {
    public BacktrackingListParser(String sentence) {
        super(new ListLexer(sentence));
    }

    public void parse() {
        if(speculate(this::listAssignment)) {
            listAssignment();
        } else {
            list();
        }
        match(eof);
    }

    private void list() {
        match(leftBracket);
        if(is(variable, leftBracket)) {
            items();
        }
        match(rightBracket);
    }

    private void items() {
        item();
        while (is(comma)) {
            match(comma);
            item();
        }
    }

    private void item() {
        if (is(leftBracket)) {
            list();
        } else if(is(1, variable) && is(2, equals)) {
            assignment();
        } else {
            match(variable);
        }
    }

    private void assignment() {
        match(variable);
        match(equals);
        match(variable);
    }

    private void listAssignment() {
        list();
        match(equals);
        list();
    }
}

/**
 * TODO: find a way to integrate with Parser, and maybe a VariableLookaheadBuffer
 */
class Base {
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    private final ListLexer lexer;

    private final LinkedList<Token<ListTokenType>> buffer = new LinkedList<>() ;
    private final Stack<Integer> marks = new Stack<>();

    Base(ListLexer lexer) {
        this.lexer = lexer;
    }

    private void consume() {
        if(isSpeculating()) {
            incrementHead();
        } else {
            fill(1);
            buffer.removeFirst();
        }
    }

    private void incrementHead() {
        int mark = marks.pop();
        marks.push(mark + 1);
    }

    protected void set() {
        marks.push(head());
    }

    protected void reset() {
        marks.pop();
    }

    protected boolean speculate(Speculation speculation) {
        set();
        try {
            speculation.speculate();
            return true;
        } catch(ParserException exception) {
            return false;
        } finally {
            reset();
        }
    }

    private int head() {
        if(marks.empty()) {
            return 0;
        } else {
            return marks.peek();
        }
    }

    private boolean isSpeculating() {
        return !marks.empty();
    }

    private void fill(int size) {
        if(size < 1) {
            throw new InvalidPeekPosition(size);
        }
        for(int i = buffer.size(); i < size; i++) {
            buffer.addLast(lexer.next());
        }
    }

    private Token<ListTokenType> peek(int position) {
        int headPosition = position + head();
        fill(headPosition);
        return buffer.get(headPosition - 1);
    }

    protected boolean is(int position, ListTokenType... expectedTokenTypes) {
        for(ListTokenType expectedTokenType : expectedTokenTypes) {
            if(peek(position).type == expectedTokenType) {
                return true;
            }
        }
        return false;
    }

    protected boolean is(ListTokenType... expectedTokenTypes) {
        return is(1, expectedTokenTypes);
    }

    protected void match(ListTokenType expectedTokenType) {
        if(is(expectedTokenType)) {
            consume();
        } else {
            throw new TokenMismatchException(peek(1), expectedTokenType);
        }
    }
}

@FunctionalInterface
interface Speculation {
    void speculate() throws ParserException;
}