package be.rivendale.math;

import be.rivendale.parser.lexer.rule.Rule;
import be.rivendale.parser.lexer.rule.RuleBasedLexer;

import java.util.List;

import static be.rivendale.math.MathTokenType.*;

public class MathLexer extends RuleBasedLexer<MathTokenType> {
    public MathLexer(String sentence) {
        super(sentence);
    }

    @Override
    protected void populateRules(List<Rule<MathTokenType>> rules) {
        rules.add(new Rule<>(lexer -> lexer.isEqual('\0'), lexer -> single(eof)));
        rules.add(new Rule<>(lexer -> lexer.isEqual('('), lexer -> single(leftParenthesis)));
        rules.add(new Rule<>(lexer -> lexer.isEqual(')'), lexer -> single(rightParenthesis)));
        rules.add(new Rule<>(lexer -> lexer.isEqual('+'), lexer -> single(plus)));
        rules.add(new Rule<>(lexer -> lexer.isEqual('-'), lexer -> single(minus)));
        rules.add(new Rule<>(lexer -> lexer.isEqual('*'), lexer -> single(multiply)));
        rules.add(new Rule<>(lexer -> lexer.isEqual('/'), lexer -> single(divide)));
        rules.add(new Rule<>(lexer -> isNumerical(), lexer -> sequence(lexer::isNumerical, number)));
    }
}
