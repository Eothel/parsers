package be.rivendale.math;

public enum MathTokenType {
    eof,
    leftParenthesis,
    rightParenthesis,
    multiply,
    plus,
    minus,
    divide,
    number
}
