package be.rivendale.parser.lexer.rule;

import be.rivendale.parser.lexer.Lexer;
import be.rivendale.parser.lexer.Token;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class RuleBasedLexer<TOKEN_TYPE extends Enum<?>> extends Lexer<TOKEN_TYPE> {
    protected RuleBasedLexer(String sentence) {
        super(sentence);
    }

    protected abstract void populateRules(List<Rule<TOKEN_TYPE>> rules);

    private Collection<Rule<TOKEN_TYPE>> rules() {
        ArrayList<Rule<TOKEN_TYPE>> rules = new ArrayList<>();
        populateRules(rules);
        return rules;
    }

    @Override
    protected Token<TOKEN_TYPE> nextNonWhitespaceToken() {
        for(Rule<TOKEN_TYPE> rule : rules()) {
            if(rule.predict(this)) {
                return rule.execute(this);
            }
        }
        return null;
    }
}
