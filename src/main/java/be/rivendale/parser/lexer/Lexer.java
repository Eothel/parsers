package be.rivendale.parser.lexer;

import java.util.function.BooleanSupplier;

public abstract class Lexer<TOKEN_TYPE extends Enum<?>> {
    private static final char NUL = '\0';
    private String sentence;
    private int lookAheadIndex = 0;

    protected Lexer(String sentence) {
        if(sentence == null) {
            throw new NullPointerException("Input sentence must not be null");
        }
        this.sentence = sentence;
    }

    protected char peek() {
        if(lookAheadIndex >= sentence.length()) {
            return NUL;
        } else {
            return sentence.charAt(lookAheadIndex);
        }
    }

    protected void consume() {
        lookAheadIndex++;
    }

    public Token<TOKEN_TYPE> sequence(BooleanSupplier predicate, TOKEN_TYPE type) {
        int startPosition = lookAheadIndex;
        StringBuilder buffer = new StringBuilder();
        while(!isNul() && predicate.getAsBoolean()) {
            buffer.append(peek());
            consume();
        }
        return new Token<>(type, startPosition, buffer.toString());
    }

    public Token<TOKEN_TYPE> single(TOKEN_TYPE type) {
        int startPosition = lookAheadIndex;
        String lexeme = String.valueOf(peek());
        consume();
        return new Token<>(type, startPosition, lexeme);
    }

    private void skip(BooleanSupplier predicate) {
        while(predicate.getAsBoolean()) {
            consume();
        }
    }

    public boolean isNul() {
        return isEqual(NUL);
    }

    public boolean isEqual(char character) {
        return peek() == character;
    }

    public boolean isWhitespace() {
        return isAny(" \t\n\r");
    }

    public boolean isNumerical() {
        return isAny("0123456789");
    }

    public boolean isAlphabetical() {
        char c = peek();
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
    }

    public boolean isAlphanumerical() {
        return isNumerical() || isAlphabetical();
    }

    public boolean isAny(String characters) {
        return characters.indexOf(peek()) != -1;
    }

    private LexerException newLexerException() {
        throw new LexerException(peek(), lookAheadIndex);
    }

    public Token<TOKEN_TYPE> next() {
        if(isWhitespace()) {
            skip(this::isWhitespace);
            return next();
        } else {
            Token<TOKEN_TYPE> token = nextNonWhitespaceToken();
            if(token == null) {
                throw newLexerException();
            } else {
                return token;
            }
        }
    }

    protected abstract Token<TOKEN_TYPE> nextNonWhitespaceToken();
}
