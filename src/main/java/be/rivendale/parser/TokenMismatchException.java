package be.rivendale.parser;

import be.rivendale.parser.lexer.Token;

import java.util.Arrays;

public class TokenMismatchException extends ParserException {
    public <TOKEN_TYPE extends Enum<?>> TokenMismatchException(Token<TOKEN_TYPE> actualToken, TOKEN_TYPE... expectedTokenTypes) {
        super("Invalid token at character %d: expected any of '%s' but was '%s'",
            actualToken.position,
            Arrays.toString(expectedTokenTypes),
            actualToken
        );
    }
}
