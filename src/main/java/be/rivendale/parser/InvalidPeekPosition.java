package be.rivendale.parser;

public class InvalidPeekPosition extends ParserException {
    public InvalidPeekPosition(int position) {
        super("Peek position must be > 0 but was '%d'", position);
    }

    public InvalidPeekPosition(int position, int maximum) {
        super("Peek position must be in range (0 < position <= %d), but was %d", maximum, position);
    }
}
